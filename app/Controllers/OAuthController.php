<?php

namespace App\Controllers;

use App\Models\OAuthModel;
use App\Services\OAuth;
use OAuth2\Request;

class OAuthController extends BaseController
{
    private $OAuthModel;
    private $OAuth;

    public function __construct()
    {
        $this->OAuth = new OAuth();
        $this->OAuthModel = new OAuthModel();
        
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
    }

    public function authorize()
    {
        $request = Request::createFromGlobals();
        $this->OAuth->server->handleTokenRequest($request)->send();
    }

    public function user()
    {
        if ($this->OAuth->isLoggedIn()) {
            return json_encode($this->OAuthModel->getUser());
        } else {
            $this->OAuth->server->getResponse()->send();
        }
    }
}
