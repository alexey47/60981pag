<?php

namespace App\Controllers;

use App\Models\ApartmentsModel;
use App\Models\UserDevicesModel;
use Aws\S3\S3Client;

class Apartments extends BaseController
{
    public function index()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        $model = new ApartmentsModel();
        $data['apartments'] = [];
        foreach ($model->getApartments() as $apartment) {
            if ($apartment['a_user_id'] == $this->ionAuth->getUserId()) {
                array_push($data['apartments'], $apartment);
            }
        }

        echo view('apartments/view_all', $this->withIon($data));
    }

    public function view($id = null)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        $model = new ApartmentsModel();
        $data['apartment'] = $model->getApartments($id);

        if (
            empty($data['apartment']) ||
            $data['apartment']['a_user_id'] != $this->ionAuth->getUserId() &&
            !$this->ionAuth->isAdmin()
        ) {
            return redirect()->to('/pages/view/access_denied');
        }

        $devices = new UserDevicesModel();
        $data['devices'] = $devices->getDevices($id);

        echo view('apartments/view', $this->withIon($data));
    }

    public function viewAllWithUsers()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        if ($this->ionAuth->isAdmin()) {
            if (!is_null($this->request->getPost('per_page'))) {
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page);
                if (is_null($per_page)) {
                    $per_page = '5';
                }
            }
            $data['per_page'] = $per_page;

            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) {
                    $search = '';
                }
            }
            $data['search'] = $search;

            helper(['form', 'url']);
            $model = new ApartmentsModel();
            $data['apartments'] = $model->getApartmentsWithUser(null, $search, $per_page);
            $data['pager'] = $model->pager;
            echo view('apartments/view_all_with_users', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('Auth.admin_permission_needed'));
            return redirect()->to('/pages/view/access_denied');
        }
    }

    // Apartments
    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('auth/login');
        }

        helper(['form']);
        //$data['validation'] = \Config\Services::validation();
        $data = [];
        echo view('apartments/create', $this->withIon($data));
    }

    public function store()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
            'country' => 'required',
            'city' => 'required',
            'address' => 'required'
        ])) {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'),
                        'secret' => getenv('S3_SECRET'),
                    ],
                ]);

                // Получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];

                // Загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'),
                    'Key' => getenv('S3_KEY') . '/apar' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new ApartmentsModel();
            $data = [
                'country' => $this->request->getPost('country'),
                'city' => $this->request->getPost('city'),
                'address' => $this->request->getPost('address'),
                'description' => $this->request->getPost('description'),
                'user_id' => $this->request->getPost('user_id'),
            ];

            if (!is_null($insert)) {
                $data['picture_url'] = $insert['ObjectURL'];
            } else {
                $data['picture_url'] = 'http://alexey47y.ddns.net/smart-home/placeholders/home_placeholder.jpg';
            }

            $model->save($data);
            session()->setFlashdata('message', lang('App.app_create_success'));
            return redirect()->to('/apartments');
        } else {
            return redirect()->to('/apartments/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);

        $model = new ApartmentsModel();
        $data['apartments'] = $model->getApartments($id);

        if (
            empty($data['apartments']) ||
            $data['apartments']['a_user_id'] != $this->ionAuth->getUserId() &&
            !$this->ionAuth->isAdmin()
        ) {
            return redirect()->to('/pages/view/access_denied');
        }

        echo view('apartments/edit', $this->withIon($data));
    }

    public function update()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
            'id' => 'required',
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
        ])) {
            $model = new ApartmentsModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'country' => $this->request->getPost('country'),
                'city' => $this->request->getPost('city'),
                'address' => $this->request->getPost('address'),
                'description' => $this->request->getPost('description'),
            ]);
            return redirect()->to('/apartments');
        } else {
            return redirect()->to('/apartments/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        $model = new ApartmentsModel();
        $data['apartments'] = $model->getApartments($id);

        if (
            empty($data['apartments'])
            || $data['apartments']['a_user_id'] != $this->ionAuth->getUserId()
            && !$this->ionAuth->isAdmin()
        ) {
            return redirect()->to('/pages/view/access_denied');
        }

        $model->delete($id);
        return redirect()->to('/apartments');
    }

    // Devices
    public function createDevice()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('auth/login');
        }

        helper(['form']);
        $data = [];
        echo view('apartments/create_device', $this->withIon($data));
    }

    public function storeDevice()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        helper(['form', 'url']);
        if ($this->request->getMethod() === 'post') {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'),
                        'secret' => getenv('S3_SECRET'),
                    ],
                ]);

                // Получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];

                // Загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'),
                    'Key' => getenv('S3_KEY') . '/devi' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }

            $model = new ApartmentsModel();
            $data = [
                'ud_device_id' => $this->request->getPost('ud_device_id'),
                'ud_apartment_id' => $this->request->getPost('ud_apartment_id'),
                'ud_name' => $this->request->getPost('ud_name'),
                'ud_description' => $this->request->getPost('ud_description'),
            ];

            if (!is_null($insert)) {
                $data['picture_url'] = $insert['ObjectURL'];
            } else {
                $data['picture_url'] = 'http://alexey47y.ddns.net/smart-home/placeholders/home_placeholder.jpg';
            }

            $model->save($data);
            session()->setFlashdata('message', lang('App.app_create_success'));
            return redirect()->to('/apartments');
        } else {
            return redirect()->to('/apartments/createDevice')->withInput();
        }
    }

    public function editDevice($id)
    {
    }

    public function updateDevice()
    {
    }

    public function deleteDevice($id)
    {
    }
}
