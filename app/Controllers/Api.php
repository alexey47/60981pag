<?php

namespace App\Controllers;

use App\Models\ApartmentsModel;
use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;
use App\Models\OAuthModel;
use Aws\S3\S3Client;

class Api extends ResourceController
{
    protected $modelName = 'App\Models\ApartmentsModel';
    protected $format = 'json';
    protected $oauth;

    private function isSiteAvailible($url)
    {
        // Проверка правильности URL
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }

        // Инициализация cURL
        $curlInit = curl_init($url);

        // Установка параметров запроса
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        // Получение ответа
        $response = curl_exec($curlInit);

        // закрываем CURL
        curl_close($curlInit);

        return $response ? true : false;
    }

    public function getApartments()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;

            $userId = $OAuthModel->getUser()->company == 'ADMIN' ?
                null :
                $OAuthModel->getUser()->id;
            $search = $this->request->getPost('search');
            $per_page = $this->request->getPost('per_page');

            $data = $this->model->getApartments(
                $userId,
                // $search,
                // $per_page
            );
            return $this->respond([
                'apartments' => $data,
                // 'pager' => $model->pager->getDetails('group1')
            ]);
        }
        $oauth->server->getResponse()->send();
    }

    public function store()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            if (
                $this->request->getMethod() === 'post' &&
                $this->validate([
                    'country' => 'required',
                    'city' => 'required',
                    'address' => 'required',
                ])
            ) {
                $data = [
                    'country' => $this->request->getPost('country'),
                    'city' => $this->request->getPost('city'),
                    'address' => $this->request->getPost('address'),
                    'description' => $this->request->getPost('description'),
                    'user_id' => $OAuthModel->getUser()->id,
                ];

                if (
                    $this->validate([
                        'picture' => 'is_image[picture]|max_size[picture,12000]',
                    ]) &&
                    $this->isSiteAvailible(getenv('S3_ENDPOINT'))
                ) {
                    $file = $this->request->getFile('picture');
                    if ($file->getSize() != 0) {
                        $s3 = new S3Client([
                            'version' => 'latest',
                            'region' => 'us-east-1',
                            'endpoint' => getenv('S3_ENDPOINT'),
                            'use_path_style_endpoint' => true,
                            'credentials' => [
                                'key' => getenv('S3_KEY'),
                                'secret' => getenv('S3_SECRET'),
                            ],
                        ]);
                        $ext = explode('.', $file->getName());
                        $ext = $ext[count($ext) - 1];
                        $insert = $s3->putObject([
                            'Bucket' => getenv('S3_BUCKET'),
                            'Key' => getenv('S3_KEY') . '/file' . time() . rand(100000, 999999) . '.' . $ext,
                            'Body' => fopen($file->getRealPath(), 'r+'),
                        ]);
                    }
                    if (!is_null($insert)) {
                        $data['picture_url'] = $insert['ObjectURL'];
                    }
                }

                $model->save($data);
                return $this->respondCreated(null, 'Apartment created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else {
            $this->oauth->server->getResponse()->send();
        }
    }

    public function delete($id = null)
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {
            $model = new ApartmentsModel();
            $model->delete($id);
            return $this->respondDeleted(null, 'Apartment deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }
}
