<?php


return [

    // User info
    'user_name'          => 'User name',
    'user_address'       => 'Address',
    'user_show'          => 'Show',
    'user_change'        => 'Edit',
    'user_delete'        => 'Delete',

    // Create, edit, etc
    'ap_country'        => 'Country',
    'ap_city'           => 'City',
    'ap_address'        => 'Address',
    'ap_description'    => 'Description',
    'ap_save'           => 'Save',
    'ap_edit'           => 'Edit',
    'ap_create'         => 'Create',
    'ap_search'         => 'Search',
    'ap_add'            => 'Add',

];
