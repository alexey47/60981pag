<?php

return [

    // Menu
    'menu_home' => 'Home',
    'menu_apartments' => 'Apartments',
    'menu_menu' => 'Menu',
    'menu_settings' => 'Settings',

    //  Footer
    'footer_terms' => 'Terms of use',
    'footer_language' => 'Language',

    // Login
    'login_login' => 'Log in',
    'login_logout' => 'Log out',

];
