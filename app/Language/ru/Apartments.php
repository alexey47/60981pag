<?php

/**
 * Name:  Auth Lang - Russian
 *
 * Author: Ben Edmunds
 * 		  ben.edmunds@gmail.com
 *         @benedmunds
 *
 * Author: Daniel Davis
 *         @ourmaninjapan
 *
 * Translation: Ievgen Sentiabov
 *         @joni-jones
 *
 * Location: http://github.com/benedmunds/ion_auth/
 *
 * Created:  03.09.2013
 *
 * Description:  Russian language file for Ion Auth views
 *
 */

return [

    // User info
    'user_name'          => 'Имя пользователя',
    'user_address'       => 'Адрес',
    'user_show'          => 'Показать',
    'user_change'        => 'Изменить',
    'user_delete'        => 'Удалить',

    // Create, edit, etc
    'ap_country'        => 'Страна',
    'ap_city'           => 'Город',
    'ap_address'        => 'Адрес',
    'ap_description'    => 'Описание',
    'ap_save'           => 'Сохранить',
    'ap_edit'           => 'Редактирование',
    'ap_create'         => 'Создание',
    'ap_search'         => 'Поиск',
    'ap_add'            => 'Добавить',

];
