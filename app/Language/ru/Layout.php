<?php

return [

    // Menu
    'menu_home' => 'Главная',
    'menu_apartments' => 'Апартаменты',
    'menu_menu' => 'Меню',
    'menu_settings' => 'Настройки',
    
    //  Footer
    'footer_terms' => 'Пользовательское соглашение',
    'footer_language' => 'Язык',

    // Login
    'login_login' => 'Вход',
    'login_logout' => 'Выход',

];
