<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use OAuth2\Storage\Pdo;

class OAuth
{
    public $server;

    function __construct()
    {
        $this->init();
    }

    private function init()
    {
        $storage = new MyPdo(
            [
                //! CHANGE THIS
                'dsn' => 'pgsql:host=' . getenv('database.default.hostname') . ';dbname=' . getenv('database.default.database'),
                'username' => getenv('database.default.username'),
                'password' => getenv('database.default.password'),
            ],
            [
                'user_table' => 'users'
            ],
        );
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }
}

// $storage = new MyPdo(
//     [
//         'dsn' => getenv('database.default.DBDriver') . '://' .
//             getenv('database.default.username') . ':' .
//             getenv('database.default.password') . '/' .
//             getenv('database.default.username'),

//         // 'dsn' => 'Postgre://postgres:qwerty@localhost/smarthome',
//         // 'dsn' => getenv('database.default.DSN'),
//         // 'username' => getenv('database.default.username'),
//         // 'password' => getenv('database.default.password'),
//     ],
//     // ['dsn' => $_ENV['database.default.DSN']],
//     ['user_table' => 'users'],
// );