<?php

namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;

    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('621362294908-tre6s1q7jk6et17qq90b2oscibimceak.apps.googleusercontent.com');
        $this->google_client->setClientSecret('ky8ZkCdKU7Bpa-X0y_gNHBAY');
        $this->google_client->setRedirectUri(base_url() . '/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }

    public function getGoogleClient()
    {
        return $this->google_client;
    }
}
