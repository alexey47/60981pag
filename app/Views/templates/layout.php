<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <title>Smart Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body class="d-flex h-100">
    <div class="cover-container d-flex w-100 h-100 mx-auto flex-column">

        <?php if ($ionAuth->loggedIn()) : ?>
            <header class="sticky-top mb-5">
                <nav class="navbar sticky-top navbar-expand-md navbar-light bg-light p-3">
                    <div class="container-fluid">
                        <a class="navbar-brand fas fa-laptop-house fa-2x" href="/"></a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-md-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="/">
                                        <?php echo lang('Layout.menu_home'); ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url() ?>/apartments">
                                        <?php echo lang('Layout.menu_apartments'); ?>
                                    </a>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <?php if (!$ionAuth->loggedIn()) : ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="<?= base_url() ?>/auth/login">
                                            <span class="fas fa fa-sign-in-alt"></span>
                                            <?= lang('Layout.login_login') ?>
                                        </a>
                                    </li>
                                <?php else : ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" area-expanded="false">
                                            <span class="fas fa fa-user-alt"></span>
                                            <?php echo $ionAuth->user()->row()->username; ?>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-dark dropdown-menu-end" area-labelledby="navbarDropdown">
                                            <li>
                                                <a class="dropdown-item" href="<?= base_url() ?>/apartments""><?php echo lang('Layout.menu_apartments'); ?></a>
                                            </li>

                                            <?php if ($ionAuth->isAdmin()) : ?>
                                                <li>
                                                    <a class=" dropdown-item" href="<?= base_url() ?>/apartments/viewallwithusers"><?php echo "View all apartments" ?></a>
                                            </li>
                                        <?php endif ?>

                                        <li>
                                            <a class="dropdown-item" href="#"><?php echo lang('Layout.menu_settings'); ?></a>
                                        </li>
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="<?= base_url() ?>/auth/logout">
                                                <span class="fas fa fa-sign-in-alt"></span>
                                                <?= lang('Layout.login_logout') ?>
                                            </a>
                                        </li>
                                        </ul>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>

                    </div>
                </nav>
            </header>
        <?php endif ?>

        <main class="px-3 my-5 my-auto">
            <?php $this->renderSection('content') ?>
        </main>

        <?php if ($ionAuth->loggedIn()) : ?>
            <footer class="bd-footer bg-light p-3 text-center mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md">
                            <p class="mb-0">© 2021-<?= date('Y') ?> Petrov Alexey</p>
                        </div>
                        <div class="col-md">
                            <a class="link-dark" href="<?= base_url() ?>/pages/view/agreement"><?php echo lang('Layout.footer_terms'); ?></a>
                        </div>
                    </div>
                </div>
            </footer>
        <?php endif ?>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>

</html>