<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">
    <h4><?php echo lang('Apartments.ap_create'); ?></h4>
    <?= form_open_multipart('apartments/store', ['class' => 'form-floating']); ?>
    <input type="hidden" name="user_id" value="<?= $ionAuth->user()->row()->id ?>">

    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="country" list="datalistOptions" placeholder="country" value="<?= old('country'); ?>">
        <datalist id="datalistOptions">
            <?php foreach (['Russia', 'USA', 'Syria', 'India', 'Poland'] as $country) : ?>
                <option value="<?php echo $country ?>">
            <?php endforeach; ?>
        </datalist>
        <label><?php echo lang('Apartments.ap_country'); ?></label>
    </div>

    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="city" placeholder="city" value="<?= old('city'); ?>">
        <label><?php echo lang('Apartments.ap_city'); ?></label>
    </div>
    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="address" placeholder="address" value="<?= old('address'); ?>">
        <label><?php echo lang('Apartments.ap_address'); ?></label>
    </div>
    <div class="form-group form-floating mb-3">
        <textarea type="text" class="form-control" name="description" placeholder="description"><?= old('description'); ?></textarea>
        <label><?php echo lang('Apartments.ap_description'); ?></label>
    </div>
    <div class="form-group mb-3">
        <input type="file" class="form-control" name="picture">
    </div>

    <div class="form-floating mb-3 d-grid">
        <button type="submit" class="btn btn-outline-dark" name="submit"><?php echo lang('Apartments.ap_save'); ?></button>
    </div>
    <?= form_close(); ?>
</div>
<?= $this->endSection() ?>