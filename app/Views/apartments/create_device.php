<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">
    <h4>CREATE DEVICE (IN PROGRESS)</h4>
    <?= form_open_multipart('apartments/storeDevice', ['class' => 'form-floating']); ?>
    <input type="hidden" name="user_id" value="<?= $ionAuth->user()->row()->id ?>">

    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="ud_device_id" list="ud_device_id_options" placeholder="ud_device_id" value="<?= old('ud_device_id'); ?>">
        <datalist id="ud_device_id_options">
            <?php foreach (['Ziaomi'] as $manufacturer) : ?>
                <option value="<?php echo $manufacturer ?>">
                <?php endforeach; ?>
        </datalist>
        <label>DEVICE ID</label>
    </div>
    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="ud_apartment_id" placeholder="ud_apartment_id" value="<?= old('ud_apartment_id'); ?>">
        <label>APARTMENT ID</label>
    </div>
    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="ud_name" placeholder="ud_name" value="<?= old('ud_name'); ?>">
        <label>NAME</label>
    </div>
    <div class="form-group form-floating mb-3">
        <textarea type="text" class="form-control" name="ud_description" placeholder="ud_description"><?= old('ud_description'); ?></textarea>
        <label>DESCRIPTION</label>
    </div>
    <div class="form-group mb-3">
        <input type="file" class="form-control" name="picture">
    </div>

    <div class="form-floating mb-3 d-grid">
        <button type="submit" class="btn btn-outline-dark" name="submit"><?php echo lang('Apartments.ap_save'); ?></button>
    </div>
    <?= form_close(); ?>
</div>
<?= $this->endSection() ?>