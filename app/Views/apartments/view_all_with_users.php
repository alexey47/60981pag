<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">

    <?= form_open('apartments/viewAllWithUsers', ['style' => 'display: flex']); ?>
    <input type="text" class="form-control" name="search" placeholder="" aria-label="Search" value="<?= $search; ?>" onchange="this.form.submit()">
    <button class="btn btn-outline-dark ms-3" type="submit"><?php echo lang('Apartments.ap_search'); ?></button>
    <?= form_close() ?>

    <?php if (!empty($apartments) && is_array($apartments)) : ?>
        <table class="table table-striped">
            <thead>
                <th scope="col"><?= lang('Apartments.user_name') ?></th>
                <th scope="col"><?= lang('Apartments.user_address') ?></th>
                <th scope="col"></th>
            </thead>
            <tbody>
                <?php foreach ($apartments as $item) : ?>

                    <tr>
                        <div class="text-center">
                            <td>
                                <?= esc($item['u_username']); ?>
                            </td>
                            <td>
                                <?= esc($item['a_country']); ?><br>
                                <?= esc($item['a_city']); ?><br>
                                <?= esc($item['a_address']); ?>
                            </td>
                            <td>
                                <a class="btn btn-outline-dark dropdown-toggle" href="#" id="controlDropdown" role="button" data-bs-toggle="dropdown" area-expanded="false"></a>
                                <ul class="dropdown-menu dropdown-menu-dark" area-labelledby="controlDropdown">
                                    <a class="dropdown-item" href="<?= base_url() ?>/apartments/view/<?= esc($item['a_id']); ?>">
                                        <?= lang('Apartments.user_show') ?>
                                    </a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/apartments/edit/<?= esc($item['a_id']); ?>">
                                        <?= lang('Apartments.user_change') ?>
                                    </a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/apartments/delete/<?= esc($item['a_id']); ?>">
                                        <?= lang('Apartments.user_delete') ?>
                                    </a>
                                </ul>
                            </td>
                        </div>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="container">
            <div class="row justify-content-between">
                <?= form_open('apartments/viewAllWithUsers', ['class' => 'col-auto']); ?>
                <select class="form-select" name="per_page" aria-label="per_page" onchange="this.form.submit()">
                    <option value="5" <?php if ($per_page == '5') echo ("selected"); ?>>5</option>
                    <option value="10" <?php if ($per_page == '10') echo ("selected"); ?>>10</option>
                    <option value="25" <?php if ($per_page == '25') echo ("selected"); ?>>25</option>
                    <option value="50" <?php if ($per_page == '50') echo ("selected"); ?>>50</option>
                </select>
                <?= form_close() ?>
                <div class="col-auto">
                    <?= $pager->links('group1', 'my_page'); ?>
                </div>
            </div>
        </div>

    <?php endif ?>
</div>
<?= $this->endSection() ?>