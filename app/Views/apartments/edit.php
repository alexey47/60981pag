<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <h4><?php echo lang('Apartments.ap_edit'); ?></h4>
    <?= form_open_multipart('apartments/update'); ?>
    
    <input type="hidden" name="id" value="<?= $apartments["a_id"] ?>">

    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="country" placeholder="." value="<?= $apartments["a_country"]; ?>">
        <label for="country"><?php echo lang('Apartments.ap_country'); ?></label>
        <div class="invalid-feedback">

        </div>
    </div>
    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="city" placeholder="." value="<?= $apartments["a_city"]; ?>">
        <label for="city"><?php echo lang('Apartments.ap_city'); ?></label>
        <div class="invalid-feedback">

        </div>
    </div>
    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" name="address" placeholder="." value="<?= $apartments["a_address"]; ?>">
        <label for="address"><?php echo lang('Apartments.ap_address'); ?></label>
        <div class="invalid-feedback">

        </div>
    </div>
    <div class="form-group form-floating mb-3">
        <textarea type="text" class="form-control" name="description" placeholder="." value="<?= $apartments["a_description"]; ?>"></textarea>
        <label for="description"><?php echo lang('Apartments.ap_description'); ?></label>
        <div class="invalid-feedback">

        </div>
    </div>
    <div class="form-floating mb-3 d-grid">
        <button type="submit" class="btn btn-dark" name="submit"><?php echo lang('Apartments.ap_save'); ?></button>
    </div>
    <?= form_close(); ?>

</div>
<?= $this->endSection() ?>