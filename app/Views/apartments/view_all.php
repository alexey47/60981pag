<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="album py-5">
    <div class="container">
        <?php if (!empty($apartments) && is_array($apartments)) : ?>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <?php foreach ($apartments as $item) : ?>
                    <div class="col">
                        <div class="card shadow">
                            <div style="height: 0; padding-top: 75%; position:relative; display:block;">
                                <img class="card-img-top" src="<?= esc($item['a_picture_url']) ?>" alt="img" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); max-height: 100%; max-width: 100%">
                            </div>
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="my-0"><?php echo lang('Apartments.ap_country'); ?>:</div>
                                    <div class="text-muted">
                                        <img src="<?= esc($item['c_flag_img_url']); ?>" style="height: 25px">
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0"><?php echo lang('Apartments.ap_city'); ?>:</div>
                                    <div class="text-muted">
                                        <?= esc($item['a_city']); ?>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0"><?php echo lang('Apartments.ap_address'); ?>:</div>
                                    <div class="text-muted text-truncate">
                                        <?= esc($item['a_address']); ?>
                                    </div>
                                </div>
                            </div>
                            <a class="stretched-link" href="<?= base_url() ?>/apartments/view/<?= esc($item['a_id']) ?>"></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif ?>
        <div class="form-floating my-3 d-grid">
            <a class="btn btn-outline-dark" href="<?= base_url() ?>/apartments/create"><?php echo lang('Apartments.ap_add'); ?></a>
        </div>
    </div>
</div>

<?= $this->endSection() ?>