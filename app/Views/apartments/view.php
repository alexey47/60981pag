<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>


<div class="album py-5">
    <div class="container main">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <?php foreach ($devices as $device) : ?>
                <div class="col">
                    <div class="card shadow">



                        <div style="height: 0; padding-top: 75%; position:relative; display:block;">
                            <img class="card-img-top" src="http://alexey47y.ddns.net:44444/smart-home/placeholders/device_placeholder.jpg" alt="img" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); max-height: 100%; max-width: 100%">
                        </div>
                        <div class="card-body">

                            <div class="form-floating d-grid">
                                <div class="btn-group">
                                    <button id="btnSettings" type="button" class="btn btn-outline-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><?= esc($device['ud_name']); ?></button>
                                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="btnSettings">
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/apartments/deleteDevice/<?= esc($device['ud_id']); ?>">DELETE</a></li>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/apartments/editDevice/<?= esc($device['ud_id']); ?>">EDIT</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="d-flex my-2 justify-content-between">
                                <div class="text-muted text-truncate">
                                    <?= esc($device['d_manufacturer']); ?> <?= esc($device['d_name']); ?>
                                </div>
                            </div>

                            <div class="form-floating my-3 d-grid">
                                <a class="btn btn-outline-dark" href="#">ON/OFF</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="form-floating my-3 d-grid">
            <a class="btn btn-outline-dark" href="<?= base_url() ?>/apartments/createDevice"><?php echo lang('Apartments.ap_add'); ?></a>
        </div>
    </div>
</div>

<?= $this->endSection() ?>