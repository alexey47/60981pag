<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container mb-3">
    <div class="row justify-content-center">
        <div class="card p-0" style="width: 22rem">
            <div class="card-header text-center text-muted">
                <h1><?php echo lang('Auth.login_heading'); ?></h1>
            </div>
            <div class="card-body">
                <p><?php echo lang('Auth.login_subheading'); ?></p>
                <?php if (isset($message)) : ?>
                    <?php echo $message; ?>
                <?php endif ?>

                <?php echo form_open('auth/login', ['class' => 'form-floating']); ?>
                <div class="form-floating mb-3">
                    <?php echo form_input($identity, '', 'class="form-control" placeholder="identity" required'); ?>
                    <label><?= lang('Auth.login_identity_label'); ?></label>
                </div>
                <div class="form-floating">
                    <?php echo form_input($password, '', 'class="form-control" placeholder="password" required'); ?>
                    <label><?= lang('Auth.login_password_label'); ?></label>
                </div>
                <div class="form-floating mb-3 text-end">
                    <a class="link-dark" href="forgot_password">
                        <?php echo lang('Auth.login_forgot_password'); ?>
                    </a>
                </div>
                <div class="form-floating mb-3 d-grid">
                    <?php echo form_submit('submit', lang('Auth.login_submit_btn'), 'class="btn btn-outline-dark"'); ?>
                </div>

                <div class="form-floating mb-3 d-grid">
                    <a href="<?= $authUrl; ?>" class="btn btn-outline-dark" role="button">
                        <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
                        <?= lang('Auth.login_google_btn') ?>
                    </a>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="card-footer text-muted">
                <p class="text-center">
                    <?php echo lang('Auth.register_user_message'); ?>
                    <a class="link-dark" href="register_user">
                        <?php echo lang('Auth.register_user_link'); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>