<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="card p-0" style="width: 22rem;">

                <div class="card-header text-center text-muted">
                    <h1><?php echo lang('Auth.register_user_heading'); ?></h1>
                </div>
                <div class="card-body">
                    <p><?php echo lang('Auth.create_user_subheading'); ?></p>
                    <?php if (isset($message)): ?>
                        <div class="alert alert-danger text-right" role="alert">
                            <?php echo $message; ?>
                        </div>
                    <?php endif ?>
                    <?php echo form_open('auth/register_user'); ?>
                    <div class="form-floating mb-3">
                        <?php echo form_input($first_name, '', 'class="form-control" placeholder="first name"'); ?>
                        <label><?= lang('Auth.create_user_fname_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3">
                        <?php echo form_input($last_name, '', 'class="form-control" placeholder="last name"'); ?>
                        <label><?= lang('Auth.create_user_lname_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3">
                        <?php echo form_input($email, '', 'class="form-control" placeholder="email"'); ?>
                        <label><?= lang('Auth.create_user_email_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3">
                        <?php echo form_input($phone, '', 'class="form-control" placeholder="phone"'); ?>
                        <label><?= lang('Auth.create_user_phone_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3">
                        <?php echo form_input($password, '', 'class="form-control" placeholder="password"'); ?>
                        <label><?= lang('Auth.create_user_password_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3">
                        <?php echo form_input($password_confirm, '', 'class="form-control" placeholder="password confirm"'); ?>
                        <label><?= lang('Auth.create_user_password_confirm_label'); ?></label>
                    </div>
                    <div class="form-floating mb-3 d-grid">
                        <?php echo form_submit('submit', lang('Auth.register_user_submit_btn'), 'class="btn btn-outline-dark"'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="card-footer text-center text-muted"></div>

            </div>
        </div>
    </div>

<?= $this->endSection() ?>