<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container mb-3">
        <div class="row justify-content-center">
            <div class="card p-0" style="width: 22rem">
                <div class="card-header text-center text-muted">
                    <h1><?php echo lang('Auth.forgot_password_heading'); ?></h1>
                </div>
                <div class="card-body">
                    <p><?php echo sprintf(lang('Auth.forgot_password_subheading'), $identity_label); ?></p>
                    <?php if (isset($message)): ?>
                        <div class="alert alert-danger" id="infoMessage" role="alert">
                            <?php echo $message; ?>
                        </div>
                    <?php endif ?>

                    <?php echo form_open('auth/forgot_password'); ?>
                    <div class="form-floating mb-3">
                        <?php echo form_input($identity, '', 'class="form-control" placeholder="email" required'); ?>
                        <label><?= (($type === 'email') ?
                                sprintf(lang('Auth.forgot_password_email_label'), $identity_label) :
                                sprintf(lang('Auth.forgot_password_identity_label'), $identity_label)); ?></label>
                    </div>
                    <div class="form-floating mb-3 d-grid">
                        <?php echo form_submit('submit', lang('Auth.forgot_password_submit_btn'), 'class="btn btn-outline-dark"'); ?>
                    </div>
                    <?php echo form_close(); ?>

                </div>
                <div class="card-footer">

                </div>

            </div>
        </div>
    </div>

<?= $this->endSection() ?>