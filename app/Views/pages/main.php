<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="text-center p-5">

    <h1 class="display-2"><?php echo lang('Main.main_app_name'); ?></h1>
    <p class="lead"><?php echo lang('Main.main_message'); ?></p>

    <?php if (!$ionAuth->loggedIn()) : ?>
        <a class="btn btn-outline-dark btn-lg" href="<?= base_url(); ?>/auth/login" role="button">
            <?php echo lang('Main.main_log_in'); ?>
        </a>
    <?php endif ?>


</div>
<?= $this->endSection() ?>