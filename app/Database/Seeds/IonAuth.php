<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class IonAuth extends Seeder
{
    public function run()
    {
        $groups = [
            [
                'name' => 'admin',
                'description' => 'Administrator',
            ],
            [
                'name' => 'members',
                'description' => 'General User',
            ]
        ];
        foreach ($groups as $group) {
            $this->db->table('groups')->insert($group);
        }

        $users = [
            [
                'ip_address' => '127.0.0.1',
                'username' => 'administrator',
                'password' => '$2y$12$/OhHP8SaxCoTX4.nsyRHzeQZb71AnTmZ04eZZ6bFEBIWJvYUv9LHG',
                'email' => 'admin@admin.com',
                'activation_selector' => '',
                'activation_code' => '',
                'forgotten_password_selector' => NULL,
                'forgotten_password_code' => NULL,
                'forgotten_password_time' => NULL,
                'remember_selector' => NULL,
                'remember_code' => NULL,
                'created_on' => '1268889823',
                'last_login' => '1268889823',
                'active' => '1',
                'first_name' => 'Admin',
                'last_name' => 'istrator',
                'company' => 'ADMIN',
                'phone' => '0'
            ],
            [
                'ip_address' => '127.0.0.1',
                'username' => 'example@g.com',
                'password' => '$2y$10$zrw.A1OGCNTDnXTsnGnd6u7j1sPq2Q./MkVHUkKWTgEao61Evt7ry',
                'email' => 'example@g.com',
                'activation_selector' => '',
                'activation_code' => '',
                'forgotten_password_selector' => NULL,
                'forgotten_password_code' => NULL,
                'forgotten_password_time' => NULL,
                'remember_selector' => NULL,
                'remember_code' => NULL,
                'created_on' => '1618855569',
                'last_login' => NULL,
                'active' => '1',
                'first_name' => 'Ryan',
                'last_name' => 'Osling',
                'company' => '',
                'phone' => ''
            ],
            [
                'ip_address' => '127.0.0.1',
                'username' => 'example1@g.com',
                'password' => '$2y$10$d4V21SuejxI.U1/mv6oiuOILUmttr4dS0xW9jvrxDZxvq.HcbcL.6',
                'email' => 'example1@g.com',
                'activation_selector' => '',
                'activation_code' => '',
                'forgotten_password_selector' => NULL,
                'forgotten_password_code' => NULL,
                'forgotten_password_time' => NULL,
                'remember_selector' => NULL,
                'remember_code' => NULL,
                'created_on' => '1618855731',
                'last_login' => NULL,
                'active' => '1',
                'first_name' => 'Ivan',
                'last_name' => 'Ivanov',
                'company' => '',
                'phone' => ''
            ],
            [
                'ip_address' => '127.0.0.1',
                'username' => 'example2@g.com',
                'password' => '$2y$10$aGePt1emMp256G2zHt3POeQbLkIfqFZu.XeE.b3N0u3yvVU9aMgUC',
                'email' => 'example2@g.com',
                'activation_selector' => '',
                'activation_code' => '',
                'forgotten_password_selector' => NULL,
                'forgotten_password_code' => NULL,
                'forgotten_password_time' => NULL,
                'remember_selector' => NULL,
                'remember_code' => NULL,
                'created_on' => '1618855800',
                'last_login' => NULL,
                'active' => '1',
                'first_name' => 'Umar',
                'last_name' => 'ibn-La\'Ahad',
                'company' => '',
                'phone' => ''
            ],
            [
                'ip_address' => '127.0.0.1',
                'username' => 'example5@g.com',
                'password' => '$2y$10$1yYNmbQzafGVWG4GqLMPOujH.eVOjmyU70oRTJTu8RnxvUge9ire2',
                'email' => 'example5@g.com',
                'activation_selector' => '',
                'activation_code' => '',
                'forgotten_password_selector' => NULL,
                'forgotten_password_code' => NULL,
                'forgotten_password_time' => NULL,
                'remember_selector' => NULL,
                'remember_code' => NULL,
                'created_on' => '1619094377',
                'last_login' => NULL,
                'active' => '1',
                'first_name' => 'Hugh',
                'last_name' => 'Kekman',
                'company' => '',
                'phone' => ''
            ],
        ];
        foreach ($users as $user) {
            $this->db->table('users')->insert($user);
        }

        $users_groups = [
            [
                'user_id' => 1,
                'group_id' => 1
            ],
            [
                'user_id' => 1,
                'group_id' => 2
            ],
            [
                'user_id' => 2,
                'group_id' => 2
            ],
            [
                'user_id' => 3,
                'group_id' => 2
            ],
            [
                'user_id' => 4,
                'group_id' => 2
            ],
            [
                'user_id' => 5,
                'group_id' => 2
            ],
        ];
        foreach ($users_groups as $users_group) {
            $this->db->table('users_groups')->insert($users_group);
        }
    }
}
