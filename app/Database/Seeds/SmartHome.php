<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SmartHome extends Seeder
{
    public function run()
    {
        $countries = [
            [
                'name' => 'India',
                'flag_img_url' => 'http://alexey47y.ddns.net/smart-home/flags/246-india.svg'
            ],
            [
                'name' => 'Poland',
                'flag_img_url' => 'http://alexey47y.ddns.net/smart-home/flags/211-poland.svg'
            ],
            [
                'name' => 'Russia',
                'flag_img_url' => 'http://alexey47y.ddns.net/smart-home/flags/248-russia.svg'
            ],
            [
                'name' => 'Syria',
                'flag_img_url' => 'http://alexey47y.ddns.net/smart-home/flags/022-syria.svg'
            ],
            [
                'name' => 'USA',
                'flag_img_url' => 'http://alexey47y.ddns.net/smart-home/flags/226-united-states.svg'
            ],
        ];
        foreach ($countries as $country) {
            $this->db->table('countries')->insert($country);
        }

        $apartments = [
            [
                'country' => 'Russia',
                'city' => 'Moscow',
                'address' => 'Nizhegorodskaya 46, 35',
                'description' => '',
                'user_id' => 1,
            ],
            [
                'country' => 'USA',
                'city' => 'Los Angeles',
                'address' => 'Labrador St. 18951',
                'description' => '',
                'user_id' => 2
            ],
            [
                'country' => 'India',
                'city' => 'New Delhi',
                'address' => 'Jor Bagh 10',
                'description' => '',
                'user_id' => 3
            ],
            [
                'country' => 'Russia',
                'city' => 'Vladivostok',
                'address' => 'Okeanskiy 26',
                'description' => '',
                'user_id' => 3,
            ],
            [
                'country' => 'Russia',
                'city' => 'Vladivostok',
                'address' => '50 Let Vlksm',
                'description' => '',
                'user_id' => 2,
            ],
            [
                'country' => 'Poland',
                'city' => 'Warsaw',
                'address' => 'Wilcza 27',
                'description' => '',
                'user_id' => 4,
            ],
            [
                'country' => 'USA',
                'city' => 'Los Angeles',
                'address' => 'W 84th St. 129',
                'description' => '',
                'user_id' => 2,
            ],
            [
                'country' => 'USA',
                'city' => 'Miami',
                'address' => '485 Brickell Ave, 205',
                'description' => '',
                'user_id' => 4,
            ],
            [
                'country' => 'Syria',
                'city' => 'Masyaf',
                'address' => '',
                'description' => '',
                'user_id' => 5,
            ],
        ];
        foreach ($apartments as $apartment) {
            $this->db->table('apartments')->insert($apartment);
        }

        $devices = [
            [
                'manufacturer' => 'Ziaomi',
                'name' => 'Wee Smart Kettle'
            ],
            [
                'manufacturer' => 'Ziaomi',
                'name' => 'Smartwee Zhiwee Air Humidifier 2'
            ],
            [
                'manufacturer' => 'Ziaomi',
                'name' => 'Аqara Smart Wireless Switch'
            ],
        ];
        foreach ($devices as $device) {
            $this->db->table('devices')->insert($device);
        }

        $user_devices = [
            [
                'name' => 'Kettle',
                'description' => '',
                'apartment_id' => 4,
                'device_id' => 1,
            ],
            [
                'name' => 'Kettle',
                'description' => '',
                'apartment_id' => 2,
                'device_id' => 1,
            ],
            [
                'name' => 'Switcher',
                'description' => '',
                'apartment_id' => 6,
                'device_id' => 3,
            ],
            [
                'name' => 'Switcher',
                'description' => '',
                'apartment_id' => 7,
                'device_id' => 3,
            ],
            [
                'name' => 'Выключатель',
                'description' => '',
                'apartment_id' => 4,
                'device_id' => 3,
            ],
            [
                'name' => 'Выключатель',
                'description' => '',
                'apartment_id' => 5,
                'device_id' => 3,
            ],
            [
                'name' => 'Air Humidifier',
                'description' => '',
                'apartment_id' => 9,
                'device_id' => 2,
            ],
            [
                'name' => 'Air Humidifier',
                'description' => '',
                'apartment_id' => 1,
                'device_id' => 2,
            ],
        ];
        foreach ($user_devices as $user_device) {
            $this->db->table('user_devices')->insert($user_device);
        }
    }
}
