<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use phpDocumentor\Reflection\PseudoTypes\False_;

class Addbshafferoauthtables extends Migration
{
	public function up()
	{
		if (!$this->db->tableexists('oauth_clients')) {
			$this->forge->addfield([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE,
				],
				'client_id' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE,
				],
				'client_secret' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE,
				],
				'redirect_uri' => [
					'type' => 'VARCHAR',
					'constraint' => '2000',
					'null' => TRUE,
				],
				'grant_types' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => TRUE,
				],
				'scope' => [
					'type' => 'VARCHAR',
					'constraint' => '4000',
					'null' => TRUE,
				],
				'user_id' => [
					'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => TRUE,
				],
			]);
			$this->forge->addkey('id', TRUE);
			$this->forge->addForeignKey('user_id', 'users', 'id', 'RESTRICT', 'RESTRICT');
			$this->forge->createtable('oauth_clients', TRUE);
		}

		if (!$this->db->tableexists('oauth_access_tokens')) {
			$this->forge->addfield([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'access_token' => [
					'type' => 'VARCHAR',
					'constraint' => '40',
					'null' => FALSE
				],
				'client_id' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE
				],
				'user_id' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE
				],
				'expires' => [
					'type' => 'TIMESTAMP',
					'null' => FALSE
				],
				'scope' => [
					'type' => 'VARCHAR',
					'constraint' => '4000',
					'null' => TRUE
				],
			]);
			$this->forge->addkey('id', TRUE);
			//$this->forge->addForeignKey('user_id','users','id','RESTRICT','RESTRICT');
			//$this->forge->addForeignKey('client_id','oauth_clients','id','RESTRICT','RESTRICT');
			$this->forge->createtable('oauth_access_tokens', TRUE);
		}

		if (!$this->db->tableexists('oauth_refresh_tokens')) {
			$this->forge->addfield([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'refresh_token' => [
					'type' => 'VARCHAR',
					'constraint' => '40',
					'null' => FALSE
				],
				'client_id' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE
				],
				'user_id' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE
				],
				'expires' => [
					'type' => 'TIMESTAMP',
					'null' => TRUE
				],
				'scope' => [
					'type' => 'VARCHAR',
					'constraint' => '4000',
					'null' => TRUE
				],
			]);
			$this->forge->addkey('id', TRUE);
			$this->forge->createtable('oauth_refresh_tokens', TRUE);
		}

		if (!$this->db->tableexists('oauth_scopes')) {
			$this->forge->addfield([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'scope' => [
					'type' => 'VARCHAR',
					'constraint' => '80',
					'null' => FALSE
				],
				'is_default' => [
					'type' => 'BOOLEAN',
					'null' => TRUE
				],
			]);
			$this->forge->addkey('id', TRUE);
			$this->forge->createtable('oauth_scopes', TRUE);
		}
	}

	public function down()
	{
		$this->forge->droptable('oauth_scopes', 'locale');
		$this->forge->droptable('oauth_refresh_tokens', 'locale');
		$this->forge->droptable('oauth_access_tokens', 'locale');
		$this->forge->droptable('oauth_clients', 'locale');
	}
}
