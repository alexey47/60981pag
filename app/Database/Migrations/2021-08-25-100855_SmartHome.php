<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SmartHome extends Migration
{
	public function up()
	{
		// Countries
		if (!$this->db->tableexists('countries')) {
			$this->forge->addField([
				'name' => [
					'type' => 'VARCHAR',
					'constraint' => '128',
					'null' => FALSE
				],
				'flag_img_url' => [
					'type' => 'text',
					'null' => FALSE
				]
			]);
			$this->forge->addKey('name', TRUE);
			$this->forge->createTable('countries', TRUE);
		}

		// Apartments
		if (!$this->db->tableexists('apartments')) {
			$this->forge->addField([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'country' => [
					'type' => 'VARCHAR',
					'constraint' => '128',
					'null' => FALSE
				],
				'city' => [
					'type' => 'VARCHAR',
					'constraint' => '128',
					'null' => FALSE
				],
				'address' => [
					'type' => 'VARCHAR',
					'constraint' => '256',
					'null' => FALSE
				],
				'description' => [
					'type' => 'TEXT',
					'null' => FALSE
				],
				'user_id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
				]
			]);
			$this->forge->addKey('id', TRUE);
			$this->forge->addForeignKey('country', 'countries', 'name', 'RESTRICT', 'RESTRICT');
			$this->forge->addForeignKey('user_id', 'users', 'id', 'RESTRICT', 'RESTRICT');
			$this->forge->createTable('apartments', TRUE);
		}

		// Devices
		if (!$this->db->tableexists('devices')) {
			$this->forge->addField([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'manufacturer' => [
					'type' => 'VARCHAR',
					'constraint' => '128',
					'null' => FALSE
				],
				'name' => [
					'type' => 'VARCHAR',
					'constraint' => '128',
					'null' => FALSE
				]
			]);
			$this->forge->addKey('id', TRUE);
			$this->forge->createTable('devices', TRUE);
		}

		// User devices
		if (!$this->db->tableexists('user_devices')) {
			$this->forge->addField([
				'id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
					'auto_increment' => TRUE
				],
				'name' => [
					'type' => 'VARCHAR',
					'constraint' => '64',
					'null' => FALSE
				],
				'description' => [
					'type' => 'text',
					'null' => FALSE
				],
				'apartment_id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
				],
				'device_id' => [
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => FALSE,
				],
			]);
			$this->forge->addKey('id', TRUE);
			$this->forge->addForeignKey('apartment_id', 'apartments', 'id', 'RESTRICT', 'RESTRICT');
			$this->forge->addForeignKey('device_id', 'devices', 'id', 'RESTRICT', 'RESTRICT');
			$this->forge->createTable('user_devices', TRUE);
		}
	}

	public function down()
	{
		$drop_order = [
			'user_devices',
			'devices',
			'apartments',
			'countries',
		];

		foreach ($drop_order as $item) {
			$this->forge->droptable($item);
		}
	}	
}
