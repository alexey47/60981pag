<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpicturefield extends Migration
{
	public function up()
	{
		// Apartments
		if ($this->db->tableexists('apartments')) {
			$this->forge->addColumn(
				'apartments',
				[
					'picture_url' => [
						'type' => 'VARCHAR',
						'constraint' => '255',
						'null' => TRUE
					]
				]
			);
		}
	}

	public function down()
	{
		$tables = [
			'apartments',
			'users'
		];

		foreach ($tables as $table) {
			$this->forge->dropColumn($table, 'picture_url');
		}
	}
}
