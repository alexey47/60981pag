<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class IonAuth extends Migration
{
    public function up()
    {
        // Groups
        if (!$this->db->tableexists('groups')) {
            $this->forge->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE
                ],
                'name' => [
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => FALSE
                ],
                'description' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => FALSE
                ]
            ]);
            $this->forge->addkey('id', TRUE);
            $this->forge->createtable('groups', TRUE);
        }

        // Users
        if (!$this->db->tableexists('users')) {
            $this->forge->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE
                ],
                'ip_address' => [
                    'type' => 'VARCHAR',
                    'constraint' => '45',
                    'null' => FALSE
                ],
                'username' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => FALSE
                ],
                'password' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => FALSE
                ],
                'email' => [
                    'type' => 'VARCHAR',
                    'constraint' => '254',
                    'null' => FALSE,
                    'unique' => 'TRUE'
                ],
                'activation_selector' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE,
                    'unique' => 'TRUE'
                ],
                'activation_code' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
                'forgotten_password_selector' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE,
                    'unique' => 'TRUE'
                ],
                'forgotten_password_code' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
                'forgotten_password_time' => [
                    'type' => 'int',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'remember_selector' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE,
                    'unique' => 'TRUE'
                ],
                'remember_code' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
                'created_on' => [
                    'type' => 'int',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'null' => FALSE
                ],
                'last_login' => [
                    'type' => 'int',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'active' => [
                    'type' => 'tinyint',
                    'constraint' => '1',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'first_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'null' => TRUE
                ],
                'last_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'null' => TRUE
                ],
                'company' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => TRUE
                ],
                'phone' => [
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => TRUE
                ],
                'google_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
                'locale' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
                'picture_url' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE
                ],
            ]);
            $this->forge->addkey('id', TRUE);
            $this->forge->createtable('users', TRUE);
        }

        // Users groups
        if (!$this->db->tableexists('users_groups')) {
            $this->forge->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE
                ],
                'user_id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE
                ],
                'group_id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE
                ]
            ]);
            $this->forge->addkey('id', TRUE);
            $this->forge->addForeignKey('user_id', 'users', 'id', 'RESTRICT', 'RESRICT');
            $this->forge->addForeignKey('group_id', 'groups', 'id', 'RESTRICT', 'RESRICT');
            $this->forge->createtable('users_groups', TRUE);
        }

        // Login attempts
        if (!$this->db->tableexists('login_attempts')) {
            $this->forge->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'auto_increment' => TRUE
                ],
                'ip_address' => [
                    'type' => 'VARCHAR',
                    'constraint' => '45',
                    'null' => FALSE
                ],
                'login' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => FALSE
                ],
                'time' => [
                    'type' => 'int',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'null' => FALSE
                ]
            ]);
            $this->forge->addkey('id', TRUE);
            $this->forge->createtable('login_attempts', TRUE);
        }
    }

    public function down()
    {
        $drop_order = [
			'login_attempts',
			'users_groups',
			'users',
			'groups',
		];
		
		foreach ($drop_order as $item) {
			$this->forge->droptable($item);
		}
    }
}
