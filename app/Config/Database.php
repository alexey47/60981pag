<?php

namespace Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class Database extends Config
{
	/**
	 * The directory that holds the Migrations
	 * and Seeds directories.
	 *
	 * @var string
	 */
	public $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

	/**
	 * Lets you choose which connection group to
	 * use if no other is specified.
	 *
	 * @var string
	 */
	public $defaultGroup = 'default';

	/**
	 * The default database connection.
	 *
	 * @var array
	 */
	// MySQL database
	// public $default = [
	// 	'DSN'      => 'mysql:host=localhost;dbname=smarthome',
	// 	'hostname' => '',
	// 	'username' => '',
	// 	'password' => '',
	// 	'database' => '',
	// 	'DBDriver' => 'MySQLi',
	// 	'DBPrefix' => '',
	// 	'pConnect' => false,
	// 	'DBDebug'  => (ENVIRONMENT !== 'production'),
	// 	'charset'  => 'utf8',
	// 	'DBCollat' => 'utf8_general_ci',
	// 	'swapPre'  => '',
	// 	'encrypt'  => false,
	// 	'compress' => false,
	// 	'strictOn' => false,
	// 	'failover' => [],
	// 	'port'     => 3306,
	// ];

	//	Postgres Heroku database
	public $default = [
		'DSN'      => 'Postgre://dfudhnwopgtytr:9a8feadf9222c36bfd0b97b4247f9f3adbee44d0eac8075007aa1fc5c4ab9b51@ec2-54-195-76-73.eu-west-1.compute.amazonaws.com:5432/d9psklai0gnann',
		'hostname' => 'ec2-54-195-76-73.eu-west-1.compute.amazonaws.com',
		'username' => 'dfudhnwopgtytr',
		'password' => '9a8feadf9222c36bfd0b97b4247f9f3adbee44d0eac8075007aa1fc5c4ab9b51',
		'database' => 'd9psklai0gnann',
		'DBDriver' => 'Postgre',
		'DBPrefix' => '',
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 5432,
	];

	//	Postgres database
	// public $default = [
	// 	'DSN'      => 'Postgre://postgres:qwerty@localhost/smarthome',
	// 	'hostname' => 'localhost',
	// 	'username' => 'postgres',
	// 	'password' => 'qwerty',
	// 	'database' => 'smarthome',
	// 	'DBDriver' => 'Postgre',
	// 	'DBPrefix' => '',
	// 	'pConnect' => false,
	// 	'DBDebug'  => (ENVIRONMENT !== 'production'),
	// 	'charset'  => 'utf8',
	// 	'DBCollat' => 'utf8_general_ci',
	// 	'swapPre'  => '',
	// 	'encrypt'  => false,
	// 	'compress' => false,
	// 	'strictOn' => false,
	// 	'failover' => [],
	// 	'port'     => 5432,
	// ];

	/**
	 * This database connection is used when
	 * running PHPUnit database tests.
	 *
	 * @var array
	 */
	public $tests = [
		'DSN'      => '',
		'hostname' => '127.0.0.1',
		'username' => '',
		'password' => '',
		'database' => ':memory:',
		'DBDriver' => 'SQLite3',
		'DBPrefix' => 'db_',  // Needed to ensure we're working correctly with prefixes live. DO NOT REMOVE FOR CI DEVS
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 3306,
	];

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();

		// Ensure that we always set the database group to 'tests' if
		// we are currently running an automated test suite, so that
		// we don't overwrite live data on accident.
		if (ENVIRONMENT === 'testing') {
			$this->defaultGroup = 'tests';
		}
	}

	//--------------------------------------------------------------------

}
