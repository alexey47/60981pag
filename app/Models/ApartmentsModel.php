<?php

namespace App\Models;

use CodeIgniter\Model;

class ApartmentsModel extends Model
{
    protected $table = 'apartments';
    protected $allowedFields = [
        'id',
        'country',
        'city',
        'address',
        'description',
        'user_id',
        'picture_url'
    ];

    public function getApartments($id = null, $search = '', $per_page = null)
    {
        $builder = $this
            ->join('countries', 'apartments.country = countries.name')
            ->select('countries.name            as c_name,
                      countries.flag_img_url    as c_flag_img_url,
                      apartments.id             as a_id,
                      apartments.country        as a_country,
                      apartments.city           as a_city,
                      apartments.address        as a_address,
                      apartments.description    as a_description,
                      apartments.user_id        as a_user_id,
                      apartments.picture_url    as a_picture_url');
        if (!isset($id)) {
            return $builder->findAll();
            // return $builder->paginate($per_page, 'group1');
        }

        $result = [];
        foreach ($builder->findAll() as $apartment) {
            if ($apartment['a_user_id'] == $id) {
                array_push($result, $apartment);
            }
        }
        return $result;
    }

    public function getApartmentsWithUser($id = null, $search = '', $per_page = null)
    {
        $builder = $this
            ->join('users', 'apartments.user_id = users.id')
            ->select('apartments.id         as a_id,
                      apartments.country    as a_country,
                      apartments.city       as a_city,
                      apartments.address    as a_address,
                      users.id              as u_id,
                      users.username        as u_username')
            ->like('username', $search, 'both', null, true);
        if (!is_null($id)) {
            return $builder->where(['apartments.id' => $id])->first();
        }
        return $builder->paginate($per_page, 'group1');
    }
}
