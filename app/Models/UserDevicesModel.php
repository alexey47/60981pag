<?php

namespace App\Models;

use CodeIgniter\Model;

class UserDevicesModel extends Model
{
    protected $table = 'user_devices';
    protected $allowedFields = [
        'id',
        'name',
        'description',
        'apartment_id',
        'device_id',
    ];

    public function getDevices($apart_id = null)
    {
        $builder = $this
            ->join('devices', 'user_devices.device_id = devices.id')
            ->select('user_devices.id           as ud_id,
                      user_devices.name         as ud_name, 
                      user_devices.description  as ud_description, 
                      user_devices.apartment_id as ud_apartment_id, 
                      user_devices.device_id    as ud_device_id,
                      devices.id                as d_id,
                      devices.manufacturer      as d_manufacturer, 
                      devices.name              as d_name');

        if (!isset($apart_id)) {
            return $builder->findAll();
        }
        return $builder->where(['user_devices.apartment_id' => $apart_id])->findAll();
    }
}
