<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Database;
use App\Services\OAuth;
use OAuth2\Request;

class OAuthModel extends Model
{
    protected $oauth;
    protected $table = 'users';

    public function getUser()
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $token = $oauth->server
                ->getAccessTokenData(Request::createFromGlobals())['access_token'];
            $query = $this->db
                ->table('oauth_access_tokens')
                ->select('*')
                ->join('users', 'oauth_access_tokens.user_id = users.username')
                ->where('oauth_access_tokens.access_token', $token)
                ->limit(1)
                ->get();

            $user = $query->getRow();
        }

        if (isset($user)) {
            return $user;
        }
    }
}
